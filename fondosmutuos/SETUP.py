import curses
import curses.textpad
from FORM_FONDOS_MUTUOS import menu_fondos
from FORM_REGISTRAR_INVERSIONISTA import menufondo
from FORM_REPORTES import mainreport


stdscr = curses.initscr()
principal = curses.newwin(44,105,0,1)

def FORM_PRINCIPAL():
	
	principal.box()
	principal.addstr(7,28,"B i e n v e n i d o s   a   F o n d o s   S U R A")
	principal. addstr(12,29,"PRESS(1) - Registrar Inversionista")
	principal. addstr(14,29,"PRESS(2) - Fondos Mutuos")
	principal. addstr(16,29,"PRESS(3) - Estado de Clientes")
	principal. addstr(22,29,"PRESS(space) - SALIR")
	principal.addstr(22,53,"(   )")
	op = principal.getch(22,55)
	principal.erase()
	principal.refresh()
	curses.endwin()
	return op

def MENU_PRINCIPAL():
	op_salir=""
	while True:
		op_salir=str(FORM_PRINCIPAL())
		if op_salir=="32":
			break
		elif op_salir=="49":		#OP1
			menufondo()
		elif op_salir=="50":		#OP2
			menu_fondos()
		elif op_salir=="51":		#OP3
			mainreport()
			

MENU_PRINCIPAL()