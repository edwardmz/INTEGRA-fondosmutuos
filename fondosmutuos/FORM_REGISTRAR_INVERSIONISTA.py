import curses
import os
import curses.textpad
from DATOS_REGISTRO import GUARDAR

registro = curses.initscr()
curses.noecho()
winregistro = curses.newwin(44,105,0,1)
winnombre = curses.newwin(1,15,7,19)
winnombre.immedok(True)
txtnombre=curses.textpad.Textbox(winnombre)

winapeparterno = curses.newwin(1,15,9,19)
winapeparterno.immedok(True)
txtapeparterno = curses.textpad.Textbox(winapeparterno)

winapematerno = curses.newwin(1,15,11,19)
winapematerno.immedok(True)
txtapematerno = curses.textpad.Textbox(winapematerno)

windni = curses.newwin(1,9,13,19)
windni.immedok(True)
txtdni = curses.textpad.Textbox(windni)

winsexo = curses.newwin(1,10,15,19)
winsexo.immedok(True)
txtsexo = curses.textpad.Textbox(winsexo)


winnumtar = curses.newwin(1,17,24,20)
winnumtar.immedok(True)
txtnumtar=curses.textpad.Textbox(winnumtar)

winfondo = curses.newwin(1,1,36,17)
winfondo.immedok(True)
txtfondo = curses.textpad.Textbox(winfondo)

wintiempoinversion = curses.newwin(1,3,38,31)
wintiempoinversion.immedok(True)
txttiempoinversion = curses.textpad.Textbox(wintiempoinversion)

wininversion = curses.newwin(1,5,40,23)
wininversion.immedok(True)
txtinversion = curses.textpad.Textbox(wininversion)


tarjeta=""
fondo=""
d1=""
d2=""
d3=""
d4=""
d5=""
d6=""
d7=""
d8=""

def label_registro():
	winregistro.box()
	winregistro.addstr(5,2,"DATOS PERSONALES")
	winregistro.addstr(7,7,"Nombres:")	
	winregistro.addstr(9,7,"Ape. Pat:")	
	winregistro.addstr(11,7,"Ape. Mat.")	
	winregistro.addstr(13,7,"DNI:")	
	winregistro.addstr(15,7,"Sexo:")	

	winregistro.addstr(17,2,"TARJETA DE DEPOSITO")
	winregistro.addstr(19,7,"1.-Scotiabank")
	winregistro.addstr(20,7,"2.-BCP")
	winregistro.addstr(21,7,"3.-BBVA")	
	winregistro.addstr(22,10,"Ingrese Opcion(  ):")
	winregistro.addstr(24,7,"N° Tarjeta:")

	winregistro.addstr(26,2,"TIPO DE FONDO")
	winregistro. addstr(28,7,"1.-SURA Ultra Cash Soles")
	winregistro. addstr(29,7,"2.-SURA Corto Plato Soles")
	winregistro. addstr(30,7,"3.-SURA Renta Soles")
	winregistro. addstr(31,7,"4.-SURA Capital Estrategico I")
	winregistro. addstr(32,7,"5.-SURA Capital Estrategico II")
	winregistro. addstr(33,7,"6.-SURA Acciones")
	winregistro. addstr(34,7,"7.-SURA Mercados Integrados")
	winregistro. addstr(35,7,"8.-SURA Acciones Europeas")	
	winregistro. addstr(36,10,"Fondo(   ):")
	winregistro. addstr(38,7,"Tiempo de Inversion:")
	winregistro. addstr(40,7,"Inversion S/.:")
	
	winregistro. addstr(5,50,"RESUMEN DE FONDO ELEGIDO")
	winregistro. addstr(6,54,"- Tipo de Fondo")
	winregistro. addstr(9,54,"- Inversion Minima")
	winregistro. addstr(12,54,"- Suscripcion Adicional")
	winregistro. addstr(15,54,"- Plazo Minimo de Pertenencia")
	winregistro. addstr(18,54,"- Vigencia de Comisiones")
	winregistro. addstr(21,54,"- Comision Unificada Anual")
	winregistro. addstr(24,54,"- Comision Por Rescate Anticipado")

	winregistro.addstr(29,50,"OPCIONES DE VENTANA")
	winregistro.addstr(31,54,"1. Ingresar Datos")
	winregistro.addstr(32,54,"2. Guardar Datos")
	winregistro.addstr(34,54,"ENTER. MENU PRINCIPAL")	
	winregistro.addstr(35,60,"Opcion(  )")
	winregistro.refresh()
	curses.endwin()

def limpiartextbox():
	winapeparterno.erase()
	winapematerno.erase()
	windni.erase()
	winsexo.erase()
	wintiempoinversion.erase()
	winnombre.erase()
	wintiempoinversion.erase()
	wininversion.erase()
	winnumtar.erase()


def menufondo():
	label_registro()
	while True:
		
		boton = winregistro.getch(35,67)
	
		if str(boton)=="49":
			winregistro.addstr(35,75,"                        ")
			winregistro.refresh()
			limpiartextbox()
			d1,d2,d3,d4,d5,d6,d7,d8,d9,d10 = ingresodatos()
		elif str(boton)=="50":
			try:
				GUARDAR(d1,d2,d3,d4,d5,d6,d7,d8,d9,d10)
				winregistro.addstr(35,75,"DATOS GUARDADOS")
				winregistro.refresh()
			except:
				winregistro.addstr(35,75,"NO HAY REGITROS")
				winregistro.refresh()

		elif str(boton)=="10":
			registro.refresh()
			curses.endwin()
			break

def ingresonombre():
	nombre = txtnombre.edit()
	nombre = txtnombre.gather().strip()
	return nombre
def ingresoapepaterno():
	paterno = txtapeparterno.edit()
	paterno = txtapeparterno.gather().strip()
	return paterno
def ingresoapematerno():
	materno = txtapematerno.edit()
	apellidomaterno = txtapematerno.gather().strip()
	return apellidomaterno
def ingresoDNI():
	dni = txtdni.edit()
	dni = txtdni.gather().strip()
	return dni
def ingresosexo():
	sexo = txtsexo.edit()
	sexo = txtsexo.gather().strip()
	return sexo
def ingresotipotarjeta():
	tipotarjeta = winregistro.getch(22,26)
	return tipotarjeta
def ingresonumerotarjeta():
	numerotarjeta = txtnumtar.edit()
	numerotarjeta = txtnumtar.gather().strip()
	return numerotarjeta
def ingresofondos():
	tipofondo = winregistro.getch(36,17)
	return tipofondo
def ingresotiempoinversion():
	tiempoinversion = txttiempoinversion.edit()
	tiempoinversion = txttiempoinversion.gather().strip()
	return tiempoinversion
def ingresoinversion():
	inversion = txtinversion.edit()
	inversion = txtinversion.gather().strip()
	return inversion

def bucletarjeta():
	tar = ingresotipotarjeta()
	if str(tar)=="49":
		winregistro.addstr(22,31,"                     ")
		winregistro.addstr(22,31,"Scotiabank")
		return 1
	elif str(tar)=="50":
		winregistro.addstr(22,31,"                     ")
		winregistro.addstr(22,31,"BCP")
		return 2
	elif str(tar)=="51":
		winregistro.addstr(22,31,"                     ")
		winregistro.addstr(22,31,"BBVA")
		return 3
	else:
		return 4

def buclefondo():
	fon = ingresofondos()
	if str(fon)=="49":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Ultra Cash Soles")
		return 1
	elif str(fon)=="50":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Corto Plato Soles")
		return 2
	elif str(fon)=="51":  
		winregistro.addstr(36,28,"                            ")
		winregistro.addstr(36,28,"SURA Renta Soles")
		return 3
	elif str(fon)=="52":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Capital Estrategico I")
		return 4
	elif str(fon)=="53":
		winregistro.addstr(36,28,"                             ")
		winregistro.addstr(36,28,"SURA Capital Estrategico II")
		return 5
	elif str(fon)=="54":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Acciones")
		return 6
	elif str(fon)=="55":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Mercados Integrados")
		return 7
	elif str(fon)=="56":
		winregistro.addstr(36,28,"                           ")
		winregistro.addstr(36,28,"SURA Acciones Europeas")
		return 8
		
def ingresodatos():
	dato1=0
	dato2=0
	dato3=0
	dato4=0
	dato5=0
	dato6=0
	dato7=0
	
	dato9=0
	dato10=0
	dato11=0
	try:
		while dato1==0:
			nom = ingresonombre()
			condi = validarletras(nom)
			if condi==True:
				dato1=1
		
		while dato2==0:
			apep = ingresoapepaterno()
			condi = validarletras(apep)
			if condi == True:
				dato2=1
			else:
				dato2=0
		while dato3==0:
			apem = ingresoapematerno()
			condi = validarletras(apem)
			if condi == True:
				dato3=1
			else:
				dato3=0
		while dato4 ==0:
			documento = ingresoDNI()
			condi = validardni(documento)
			if condi == True:
				dato4=1
			else:
				dato4=0
		while dato5 ==0:
			sexo = ingresosexo()
			if sexo=="MASCULINO" or sexo=="FEMENINO":
				dato5=1
			else:
				dato5=0
		while dato6 == 0:
			tiptar = bucletarjeta()
			if tiptar ==1 or tiptar==2 or tiptar==3:
				winregistro.refresh()
				dato6=1
			elif tiptar==4:
				dato6=0
		while dato7 ==0:
			numtar = ingresonumerotarjeta()
			if enteros(numtar)==True and len(numtar)==16:
				dato7=1
		while dato9==0:
			fondoelegido = buclefondo()
			if fondoelegido==1 or fondoelegido==2 or fondoelegido == 3 or fondoelegido == 4 or fondoelegido == 5 or fondoelegido ==6 or fondoelegido==7 or fondoelegido==8:
				winregistro.refresh()
				dato9=1

		while dato10==0:
			tiempo = ingresotiempoinversion()
			if enteros(tiempo)==True:
				dato10=1

		while dato11==0:
			inver = ingresoinversion()
			if float(inver)>2500:
				dato11=1
				return nom,apep,apem,documento,sexo,tiptar,numtar,fondoelegido,tiempo,inver
			else:
				dato11=0
	except (NameError,SystemError,SyntaxError):
		dato=1


def enteros(cadena):
	try:
		if cadena.isdigit()==True and int(cadena)>0:
			return True
		else:
			return False
	except:
		return False

def validardni(cadena):
	if cadena.isdigit()==True and len(cadena)==8:
		return True
	else:
		return False

def validarletras(cadena):

	if cadena.isalpha()==True:
		return True
	else:
		return False
	


