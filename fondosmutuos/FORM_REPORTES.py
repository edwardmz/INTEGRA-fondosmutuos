import curses
from leer import leerarchivo
import curses.textpad



reportes = curses.initscr()
winreporte = curses.newwin(44,105,0,1)

winbuscar = curses.newwin(1,9,5,22)
txtbuscar = curses.textpad.Textbox(winbuscar)
curses.noecho()

datos=[]

nombre=""
apepa=""
apema=""
dni=""
sexo=""
tarjeta=""
numtarjeta=""
fondo=""
tiempoinversion=""
inversion=""
estado=""
estadoinversion=""

winreporte.box()
	
def reporte():
	datos=leerarchivo()
	winreporte.box()
	winreporte.addstr(5,5,"BUSCAR POR DNI:")
	winreporte.addstr(10,5,"DATOS RELEVANTES DEL CLIENTE")
	winreporte.addstr(12,5,"Nombre:")
	winreporte.addstr(14,5,"Ape. Paterno:")
	winreporte.addstr(16,5,"Ape. Materno:")
	winreporte.addstr(18,5,"DNI:")
	winreporte.addstr(20,5,"Inversion S/.:")
	winreporte.addstr(22,5,"Fondo Elegido:")
	winreporte.addstr(24,5,"Numero de Cuenta:")
	winreporte.refresh()

def imprimirbusqueda(datobuscar):
	datos=leerarchivo()
	filas = len(datos)
	x=0
	while x<filas:
		if datobuscar==datos[x][3]:
			winreporte.addstr(12,24,str(datos[x][0]))
			winreporte.addstr(14,24,str(datos[x][1]))
			winreporte.addstr(16,24,str(datos[x][2]))
			winreporte.addstr(18,24,str(datos[x][3]))
			winreporte.addstr(20,24,str(datos[x][9]))
			
			winreporte.addstr(24,24,str(datos[x][6]))
			if datos[x][7]=="1":
				winreporte.addstr(22,24,"SURA Ultra Cash Soles")
			elif datos[x][7]=="2":
				winreporte.addstr(22,24,"SURA Corto Plazo Soles")
			elif datos[x][7]=="3":
				winreporte.addstr(22,24,"SURA Renta Soles")
			elif datos[x][7]=="4":
				winreporte.addstr(22,24,"SURA Capital Estrategico I")
			elif datos[x][7]=="5":
				winreporte.addstr(22,24,"SURA Capital Estrategico II")
			elif datos[x][7]=="6":
				winreporte.addstr(22,24,"SURA Acciones")
			elif datos[x][7]=="7":
				winreporte.addstr(22,24,"SURA Mercados Integrados")
			elif datos[x][7]=="8":
				winreporte.addstr(22,24,"SURA Acciones Europeas")
			return datos[x][9],datos[x][7]

		x+=1
	


def gananciafondo(inversion,lista=[]):
	
	datos=leerarchivo()
	winreporte.addstr(26,15,"2012")
	winreporte.addstr(26,28,"2013")
	winreporte.addstr(26,41,"2014")
	winreporte.addstr(26,54,"2015")
	winreporte.addstr(26,67,"2016")
	winreporte.addstr(26,80,"2017")

	winreporte.addstr(28,5,"ENE")
	winreporte.addstr(29,5,"FEB")
	winreporte.addstr(30,5,"MAR")
	winreporte.addstr(31,5,"ABR")
	winreporte.addstr(32,5,"MAY")
	winreporte.addstr(33,5,"JUN")
	winreporte.addstr(34,5,"JUL")
	winreporte.addstr(35,5,"AGO")
	winreporte.addstr(36,5,"SET")
	winreporte.addstr(37,5,"OCT")
	winreporte.addstr(38,5,"NOV")
	winreporte.addstr(39,5,"DIC")

	tar1=lista
	ganancia = []
	cont=0
	while cont<len(tar1):
		calculado=round(tar1[cont]*inversion,2)
		ganancia.append(calculado)
		cont+=1
	fila=28
	posicion=0
	while fila<=33:
		columna=15
		while columna<=80:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=13
		fila+=1

	while fila<=39:
		columna=15
		while columna<=67:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=13
		fila+=1


	winreporte.refresh()

def gananciafondo5col(inversion,lista=[]):
	datos=leerarchivo()
	winreporte.addstr(26,10,"2013")
	winreporte.addstr(26,29,"2014")
	winreporte.addstr(26,48,"2015")
	winreporte.addstr(26,67,"2016")
	winreporte.addstr(26,86,"2016")

	winreporte.addstr(28,5,"ENE")
	winreporte.addstr(29,5,"FEB")
	winreporte.addstr(30,5,"MAR")
	winreporte.addstr(31,5,"ABR")
	winreporte.addstr(32,5,"MAY")
	winreporte.addstr(33,5,"JUN")
	winreporte.addstr(34,5,"JUL")
	winreporte.addstr(35,5,"AGO")
	winreporte.addstr(36,5,"SET")
	winreporte.addstr(37,5,"OCT")
	winreporte.addstr(38,5,"NOV")
	winreporte.addstr(39,5,"DIC")

	tar1=lista
	ganancia = []
	cont=0
	while cont<len(tar1):
		calculado=round(tar1[cont]*inversion,2)
		ganancia.append(calculado)
		cont+=1
	
	fila=28
	posicion=0
	while fila<=33:
		columna=27
		while columna<=84:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1

	while fila<=34:
		columna=27
		while columna<=65:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1

	while fila<=39:
		columna=10
		while columna<=84:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1
	
	
	winreporte.refresh()

def gananciafondo5colraro(inversion,lista=[]):
	datos=leerarchivo()
	winreporte.addstr(26,10,"2013")
	winreporte.addstr(26,29,"2014")
	winreporte.addstr(26,48,"2015")
	winreporte.addstr(26,67,"2016")
	winreporte.addstr(26,86,"2017")

	winreporte.addstr(28,5,"ENE")
	winreporte.addstr(29,5,"FEB")
	winreporte.addstr(30,5,"MAR")
	winreporte.addstr(31,5,"ABR")
	winreporte.addstr(32,5,"MAY")
	winreporte.addstr(33,5,"JUN")
	winreporte.addstr(34,5,"JUL")
	winreporte.addstr(35,5,"AGO")
	winreporte.addstr(36,5,"SET")
	winreporte.addstr(37,5,"OCT")
	winreporte.addstr(38,5,"NOV")
	winreporte.addstr(39,5,"DIC")

	tar1=lista			
	ganancia = []
	cont=0
	while cont<len(tar1):
		calculado=round(tar1[cont]*inversion,2)
		ganancia.append(calculado)
		cont+=1
	
	fila=28
	posicion=0
	while fila<=33:
		columna=27
		while columna<=84:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1

	while fila<=35:
		columna=27
		while columna<=65:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1

	while fila<=39:
		columna=10
		while columna<=67:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=19
		fila+=1

	winreporte.refresh()

def gananciafondo4col(inversion,lista=[]):
	datos=leerarchivo()
	winreporte.addstr(26,20,"2014")
	winreporte.addstr(26,40,"2015")
	winreporte.addstr(26,60,"2016")
	winreporte.addstr(26,80,"2017")

	winreporte.addstr(28,10,"ENE")
	winreporte.addstr(29,10,"FEB")
	winreporte.addstr(30,10,"MAR")
	winreporte.addstr(31,10,"ABR")
	winreporte.addstr(32,10,"MAY")
	winreporte.addstr(33,10,"JUN")
	winreporte.addstr(34,10,"JUL")
	winreporte.addstr(35,10,"AGO")
	winreporte.addstr(36,10,"SET")
	winreporte.addstr(37,10,"OCT")
	winreporte.addstr(38,10,"NOV")
	winreporte.addstr(39,10,"DIC")

	tar1=lista			
	ganancia = []
	cont=0
	while cont<len(tar1):
		calculado=round(tar1[cont]*inversion,2)
		ganancia.append(calculado)
		cont+=1
	
	fila=28
	posicion=0
	while fila<=33:
		columna=40
		while columna<=80:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=20
		fila+=1

	while fila<=39:
		columna=20
		while columna<=60:
			winreporte.addstr(fila,columna,str(ganancia[posicion]))
			posicion+=1
			columna+=20
		fila+=1
	
	winreporte.refresh()

'''
def gananciafondo3(inversion):
def gananciafondo4(inversion):
def gananciafondo5(inversion):
def gananciafondo6(inversion):
def gananciafondo7(inversion):
def gananciafondo8(inversion):
'''
datos=leerarchivo()
def mainreport():

	datos=leerarchivo()
	
	lista1=[0.03,0.27,0.27,0.41,0.43,0.40,0.31,0.23,0.39,0.25,0.30,0.38,0.37,0.27,0.35,0.27,0.47,0.44,0.33,0.26,0.31,0.43,0.44,0.37,0.29,0.25,0.32,0.34,0.41,0.38,0.28,0.24,0.31,0.33,0.43,0.09,0.31,0.26,0.31,0.26,0.41,0.28,0.26,0.29,0.16,0.40,0.27,0.31,0.28,0.47,0.37,0.28,0.29,0.41,0.49,0.38,0.27,0.27,0.28,0.40,0.36,0.27,0.36,0.22,0.37,0.37]
	lista2=[0.35,0.47,0.15,0.52,0.42,0.43,0.43,0.30,0.38,0.26,0.30,0.41,0.30,0.18,0.46,0.24,0.53,0.54,0.40,0.24,0.39,0.37,0.55,0.41,0.45,-0.03,0.46,0.21,0.36,0.42,0.34,-0.32,0.45,0.21,0.55,0.12,0.41,0.33,0.44,0.29,0.48,0.36,-0.10,0.29,0.03,0.42,0.46,0.65,0.30,0.27,0.41,0.32,0.34,0.54,0.59,0.44,0.36,0.51,0.32,0.42,0.38,0.43,0.36,0.17,0.20,0.38]
	lista3=[0.27,0.26,-0.79,0.75,0.15,0.55,1.12,0.15,0.73,0.08,0.75,0.48,0.35,0.20,0.82,-0.30,0.75,0.70,0.90,0.19,0.48,0.45,0.93,0.59,0.43,-1.62,1.05,-0.36,0.27,0.52,0.92,-1.60,0.78,-0.45,0.85,0.19,0.82,0.35,0.63,0.11,0.51,0.58,-1.74,0.34,-0.63,0.32,0.59,2.07,-0.28,-0.05,0.43,0.53,0.61,1.04,0.49,0.41,0.63,0.28,0.30,0.42,-0.03,0.97,0.13,-0.05,-0.66,0.28]
	lista4=[-0.38,0.61,1.01,0.34,0.27,1.01,0.74,0.38,-0.10,-0.07,-4.27,0.90,0.69,1.15,-1.11,0.97,0.45,0.60,3.01,0.21,0.66,0.02,3.01,0.21,0.34,0.33,1.94,0.08,0.48,-0.13,0.43,0.26,-0.05,-0.71,0.42,0.75,0.59,2.76,0.14,-0.17,0.27,2.09,0.37,0.50,-0.01,0.43,0.39]	
	lista5=[-1.57,0.04,-1.74,0.06,-1.05,2.95,-1.66,0.22,-5.37,-1.27,-1.34,1.78,3.26,2.83,-1.32,1.10,-0.09,0.08,3.54,2.04,1.49,-1.76,-1.66,0.38,-0.31,0.49,4.18,1.96,-4.30,1.09,0.02,-1.65,-2.43,0.46,3.47,0.20,5.87,-1.03,-3.95,0.07,1.67,0.40,2.20,-0.82,-1.16,0.37]
	lista6=[10.83,1.67,-1.82,-7.42,-7.21,0.76,2.81,-1.55,-1.32,-3.82,14.63,-2.78,5.10,-1.83,-7.83,-5.25,14.83,3.10,-1.62,-10.87,8.21,6.29,14.65,-1.21,-7.41,-8.25,-0.72,-0.44,-2.01,1.35,-0.99,-1.29,2.27,0.23,1.02,-0.63,-4.39,-5.06,0.85,-8.25,10.58,-0.37,6.75,1.88,-12.26,-0.51,6.15,-4.17,-3.44,-2.73,-0.01,-4.26,4.08,-3.76,2.87,0.13,-2.96,-6.66,-0.73,-0.82,-2.42,2.58,3.81,-0.95,-3.66,1.75,]
	lista7=[11.67,2.17,-8.61,-8.18,-3.20,1.39,5.75,-1.61,2.22,-0.94,2.00,0.11,0.37,-2.45,1.97,-4.91,10.45,7.00,-0.57,-6.57,4.04,9.88,2.24,-0.33,-8.11,-7.37,1.16,-4.79,-6.11,0.24,-1.29,-5.54,1.50,-2.76,3.73,1.62,-0.66,-1.87,-0.87,-4.50,0.12,-0.58,1.21,0.44,-6.50,-0.04,3.20,0.91,-6.28,-1.29,-1.91,-1.46,0.32,-2.36,5.26,3.76,-3.13,-7.19,-5.17,-6.47,-9.46,4.90,0.12,-4.40,-1.74,1.06,]
	lista8=[1.16,-5.02,1.50,5.14,-3.39,0.57,-1.02,5.07,4.47,1.86,1.73,3.91,0.30,0.54,3.96,-4.89,-4.87,0.34,-3.35,3.91,3.95,0.32,-6.19,0.76,-3.60,-4.20,0.87,-2.18,6.24,-1.76,1.82,0.01,-1.72,-4.55,-3.74,5.55,]
	salir=0
	while salir ==0:


		
		reporte()
		winreporte.addstr(41,60,"ENTER - Salir / Other - Continuar")
		
		boton = winreporte.getch(41,94)
		if str(boton)=="10":
			salir=1
			break
			curses.endwin()
		else:
			winreporte.erase()
		try:
			opbuscar = txtbuscar.edit()
			opbuscar = txtbuscar.gather().strip()

			inver,fondoelegido = imprimirbusqueda(opbuscar)
			winreporte.refresh()
		except:
			continue
		if int(fondoelegido)==1:
			gananciafondo(float(inver),lista1)
		elif int(fondoelegido)==2:
			gananciafondo(float(inver),lista2)
		elif int(fondoelegido)==3:
			gananciafondo(float(inver),lista3)
		elif int(fondoelegido)==4:
			gananciafondo5col(float(inver),lista4)
		elif int(fondoelegido)==5:
			gananciafondo5colraro(float(inver),lista5)
		elif int(fondoelegido)==6:
			gananciafondo(float(inver),lista6)
		elif int(fondoelegido)==7:
			gananciafondo(float(inver),lista7)
		elif int(fondoelegido)==8:
			gananciafondo4col(float(inver),lista8)

		winreporte.refresh()

	


















